var Level2 = {
	layout   : [
		[],
		[],
		[0, 0, 'rock', 0, 0, 0, 'rock', 0, 'rock', 0],
		[0, 0, 'rock', 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 'rock', 0, 0, 0, 0, 0, 0, 'rock'],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 'rock', 0, 0, 0, 0],
		[0, 'rock', 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	],
	endPoint : 1,
	inventory: {
		pipeHorizontal : 6,
		pipeVertical   : 4,
		pipeTopLeft    : 4,
		pipeTopRight   : 4,
		pipeBottomLeft : 2,
		pipeBottomRight: 3
	}
}