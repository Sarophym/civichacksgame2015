var Level1 = {
	layout   : [
		[],
		[],
		[0, 0, 0, 0, 0, 0, 'rock', 0, 'rock', 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 'rock', 0, 0, 0, 0, 0, 0, 'rock'],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 'rock', 0, 0, 0, 0],
		[0, 'rock', 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	],
	endPoint : 3,
	inventory: {
		pipeHorizontal : 8,
		pipeVertical   : 4,
		pipeTopLeft    : 8,
		pipeTopRight   : 8,
		pipeBottomLeft : 8,
		pipeBottomRight: 8
	}
}