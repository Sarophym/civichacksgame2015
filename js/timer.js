var Timer = {
	ctx       : null,
	isPaused  : false,
	pause     : function pause() {
		if( Timer.isPaused ) { return; }
		clearInterval( Timer.updateId );
		Timer.isPaused = true;
	},
	unpause   : function unpause() {
		if( !Timer.isPaused ) { return;}
		Timer.updateId = setInterval( Timer.update, 1000 );
		Timer.isPaused = false;
	},
	time      : 0,
	updateId  : null,
	dimensions: {
		timerLeft  : 0.441,
		timerTop   : 0.391,
		timerWidth : 0.135,
		timerHeight: 0.291
	},
	init      : function init( seconds ) {
		var cv = document.getElementById( 'timer-canvas' );
		Timer.ctx = cv.getContext( '2d' );
		Timer.time = Math.min( 999, seconds || 120 );
		Timer.update( true );
		Timer.updateId = setInterval( Timer.update, 1000 );
	},
	update    : function update( justUpdate ) {
		if( !justUpdate ) {
			Timer.time = Math.max( 0, Timer.time - 1 );
		}

		Timer.ctx.clearRect( 0, 0, Game.canvasWidth, Game.canvasHeight );
		if( Timer.time === 0 && Game.state != 'losescreen' ) {
			Game.lose();
			clearInterval( Timer.updateId );
			return;
		}
		var numbers = Timer.time.toString().split( '' );
		Timer.ctx.fillStyle = '#0074D9';
		if( Timer.time < 16 ) {
			Timer.ctx.fillStyle = '#FF4136';
		}
		var fontSize = Game.canvasWidth / 1500 * 52;
		Timer.ctx.font = 'bold ' + fontSize + 'px Arial';

		var w = $( '#timer-canvas' ).attr( 'width' );
		var x = 0;
		for( var i = numbers.length; i >= 0; --i ) {
			//Render
			Timer.ctx.fillText( numbers[i], w - (Timer.dimensions.digitWidth * x) + fontSize / 2, fontSize );
			x++;
		}
	},
	cleanUp   : function cleanUp() {
		clearInterval( Timer.updateId );
		Timer.ctx.clearRect( 0, 0, Game.canvasWidth, Game.canvasHeight );
	},
	resizeMe  : function resizeMe( cWidth, cHeight ) {
		$( '#timer-canvas' ).attr( 'width', cWidth * Timer.dimensions.timerWidth );
		$( '#timer-canvas' ).attr( 'height', cHeight * Game.topGifRatio * Timer.dimensions.timerHeight );
		$( '#timer-canvas' ).css( {
			left: (Timer.dimensions.timerLeft * cWidth) + 'px',
			top : (Timer.dimensions.timerTop * cHeight * Game.topGifRatio) + 'px'
		} );
		Timer.dimensions.digitWidth = $( '#timer-canvas' ).attr( 'width' ) / 3;
	}
}