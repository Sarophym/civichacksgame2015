var timePerLevel = 60;
var Game = {
	debug        : false,
	state        : 'logo',
	topGifRatio  : 0.215,
	canvasWidth  : 0,
	canvasHeight : 0,
	tileWidth    : 16,
	unitWidth    : 12,
	unitHeight   : 8,
	renderLoopId : null,
	cursorState  : null,
	placedPipe   : [
		[2, -1] // Always the start position and our first pipe piece to validate placements from
	],
	level        : {
		layout  : [
			[],
			[],
			[0, 'pipeHorizontal', 'pipeHorizontal', 'pipeTopRight', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 0],
			[0, 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 0],
			[0, 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 0],
			[0, 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 0],
			[0, 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 0],
			[0, 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 0],
			[0, 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 'rock', 0]
		],
		endPoint: 0,
		pipeBank: {
			intersects        : function intersects( mX, mY ) {
				if( mX < this.dimensions.x || mY < this.dimensions.y
					|| mX > (this.dimensions.x + this.dimensions.width)
					|| mY > (this.dimensions.y + this.dimensions.height) ) {
					return null;
				}
				for( var i = 1; i <= 6; i++ ) {
					var iconsUp = (this.dimensions.iconSize * i);
					var gapsUp = (this.bankIconGap * i);
					if( mY < this.dimensions.y + (iconsUp + gapsUp) ) {
						for( var prop in this.inventory ) {
							if( this.inventory.hasOwnProperty( prop ) ) {
								i--;
								if( i == 0 ) {
									return prop;
								}
							}
						}
					}
				}
			},
			bankIconGap       : 0.000,
			bankHeight        : 0.59,
			bankWidth         : 0.06,
			bankTopOffset     : 0.387,
			bankLeftOffset    : 0.012,
			startRow          : 3,
			startCol          : 0.25,
			bankIconWidthRatio: 0.95,
			dimensions        : {
				x       : 0,
				y       : 0,
				width   : 0,
				height  : 0,
				bottom  : 0,
				right   : 0,
				iconSize: 0
			},
			inventory         : {
				pipeHorizontal : 0,
				pipeVertical   : 0,
				pipeTopLeft    : 0,
				pipeTopRight   : 0,
				pipeBottomLeft : 0,
				pipeBottomRight: 0
			}
		}
	},
	timer        : 120,
	images       : {
		compWin            : null,
		compLose           : null,
		compTutorial       : null,
		compGrid           : null,
		compTop            : null,
		test               : null,
		rock               : null,
		background         : null,
		pipeBottomLeft     : null,
		pipeBottomRight    : null,
		pipeHorizontal     : null,
		pipeVertical       : null,
		pipeTopLeft        : null,
		pipeTopRight       : null,
		pipeSideBottomRight: null,
		pipeSideVertical   : null
	},
	renderLevel  : function() {
		// Drawing the level
		for( var row = 0, lr = Game.level.layout.length; row < lr; row++ ) {
			for( var col = 0, lc = Game.level.layout[row].length; col < lc; col++ ) {
				var imageToDraw;
				imageToDraw = Game.images[Game.level.layout[row][col]];
				if( imageToDraw == null || imageToDraw == undefined ) { continue; }
				ctx.drawImage( imageToDraw, (col + 1) * Game.tileWidth, row * Game.tileWidth, Game.tileWidth, Game.tileWidth );
			}
		}
	},
	renderEndPipe: function() {
		// Draw the endpoint pipe
		for( var i = 0, l = Game.level.endPoint; i <= l; i++ ) {
			var imageToDraw = Game.images.pipeSideVertical;
			if( i == l ) {
				imageToDraw = Game.images.pipeSideBottomRight;
			}
			ctx.drawImage( imageToDraw, 11 * Game.tileWidth, (i + 2) * Game.tileWidth, Game.tileWidth, Game.tileWidth );
		}
	},
	renderCursor : function() {
		// Draw cursor
		if( Game.cursorState !== null ) {
			var imageToDraw;
			imageToDraw = Game.images[Game.cursorState];
			if( imageToDraw != null && imageToDraw != undefined ) {
				ctx.drawImage( imageToDraw, Game.mouseX - (Game.tileWidth / 2), Game.mouseY - (Game.tileWidth / 2), Game.tileWidth * 1.5, Game.tileWidth * 1.5 );
			}
		}
	},
	renderBank   : function renderBank() {
		var i = 0;
		var pipeBank = Game.level.pipeBank;
		var pipeInv = Game.level.pipeBank.inventory;

		for( var prop in pipeInv ) {
			if( pipeInv.hasOwnProperty( prop ) ) {
				var imageToDraw;
				imageToDraw = Game.images[prop];
				if( imageToDraw != null && imageToDraw != undefined ) {
					var x = pipeBank.bankLeftOffset * Game.canvasWidth;
					var y = i + (Game.canvasHeight * pipeBank.bankTopOffset) + (i * pipeBank.bankIconGap * Game.canvasHeight) + (i * pipeBank.dimensions.iconSize);
					var w = pipeBank.bankWidth * Game.canvasWidth * pipeBank.bankIconWidthRatio;
					var h = pipeBank.bankWidth * Game.canvasWidth * pipeBank.bankIconWidthRatio;
					//ctx.drawImage( imageToDraw, x, y, w, h );
					// Draw counts
					ctx.fillStyle = "white";
					var fontSize = 36 * (Game.canvasWidth / 768);
					ctx.font = "bold " + fontSize + "px Arial";
					ctx.fillText( pipeInv[prop], x + w, y + h );
				}
				i++;
			}
		}
	},
	renderLoop   : function renderLoop() {
		// Clear the screen
		ctx.clearRect( 0, 0, Game.canvasWidth, Game.canvasHeight );

		switch( Game.state ) {
			case 'start':
				ctx.globalAlpha = 0.5;
				ctx.drawImage( Game.images.compGrid, 0, Game.topGifRatio * Game.canvasHeight, Game.canvasWidth, Game.canvasHeight * (1 - Game.topGifRatio) );
				ctx.drawImage( Game.images.compTop, 0, 0, Game.canvasWidth, Game.canvasHeight * Game.topGifRatio );
				// 768/402
				ctx.globalAlpha = 1;
				ctx.drawImage( Game.images.compTutorial, 0.1 * Game.canvasWidth, 0.15 * Game.canvasHeight, Game.canvasWidth / 1.25, (Game.canvasWidth / 1.25) * (402 / 768) );
				$( '#comp-win-img' ).hide();
				$( '#comp-lose-img' ).hide();
				break;
			case 'ingame':
				// Draw the background
				ctx.drawImage( Game.images.compGrid, 0, Game.topGifRatio * Game.canvasHeight, Game.canvasWidth, Game.canvasHeight * (1 - Game.topGifRatio) );
				ctx.drawImage( Game.images.compTop, 0, 0, Game.canvasWidth, Game.canvasHeight * Game.topGifRatio );
				Game.renderLevel();
				Game.renderEndPipe();
				Game.renderBank();
				Game.renderCursor();
				$( '#comp-win-img' ).hide();
				$( '#comp-lose-img' ).hide();
				break;
			case 'winscreen':
				// Draw the background
				ctx.drawImage( Game.images.compGrid, 0, Game.topGifRatio * Game.canvasHeight, Game.canvasWidth, Game.canvasHeight * (1 - Game.topGifRatio) );
				ctx.drawImage( Game.images.compTop, 0, 0, Game.canvasWidth, Game.canvasHeight * Game.topGifRatio );
				Game.renderLevel();
				Game.renderEndPipe();
				Game.renderBank();
				$( '#comp-win-img' ).show();
				$( '#comp-lose-img' ).hide();
				// TODO: render winner gif
				break;
			case 'losescreen':
				$( '#comp-win-img' ).hide();
				$( '#comp-lose-img' ).show();
				// Draw the background
				ctx.drawImage( Game.images.compGrid, 0, Game.topGifRatio * Game.canvasHeight, Game.canvasWidth, Game.canvasHeight * (1 - Game.topGifRatio) );
				ctx.drawImage( Game.images.compTop, 0, 0, Game.canvasWidth, Game.canvasHeight * Game.topGifRatio );
				Game.renderLevel();
				Game.renderEndPipe();
				Game.renderBank();
			default:
		}
		// Set loop call
		requestAnimationFrame( renderLoop );
	},
	lose         : function lose() {
		Game.state = 'losescreen';
		$( '#top-gif-img' ).toggleClass( 'active inactive' );
		Game.playSound( 'lose' );
		Game.stopSound( 'bgmusic' );
		Timer.cleanUp();
	},
	playSound    : function playSound( name ) {
		switch( name ) {
			case 'build':
				$( '#construct-wav' )[0].play();
				break;
			case 'wrong':
				$( '#wrong-wav' )[0].play();
				break;
			case 'bgmusic':
				$( '#bg-music' )[0].play();
				break
			case 'victory':
				$( '#victory-wav' )[0].play();
				break;
			case 'lose':
				$( '#lose-wav' )[0].play();
				break;
			case 'pickup':
				$( '#pickup-wav' )[0].play();
				break;
			default:
		}
	},
	stopSound    : function stopSound( name ) {
		switch( name ) {
			case 'bgmusic':
				$( '#bg-music' )[0].pause();
				break
			default:
		}
	}
};

var pipeRelations = {
	'pipeHorizontal' : ['left', 'right'],
	'pipeVertical'   : ['top', 'bottom'],
	'pipeBottomLeft' : ['top', 'right'],
	'pipeBottomRight': ['top', 'left'],
	'pipeTopLeft'    : ['bottom', 'right'],
	'pipeTopRight'   : ['bottom', 'left']
};

$( document ).ready( function() {
	var cv = document.getElementById( 'main-canvas' );
	// Setup ctx object in global space O_O
	ctx = cv.getContext( '2d' );

	fitCanvasToScreen();
	assignImages();
	attachResize();
	controls();
	Game.renderLoop();
} );

function startGame( level ) {
	Game.state = 'ingame';
	loadLevel( level );
	$( '#top-gif-img' ).toggleClass( 'active inactive' );
	Timer.init( timePerLevel );
	Game.playSound( 'bgmusic' );
};

function loadLevel( level ) {
	Game.level.layout = JSON.parse( JSON.stringify( level.layout ) );
	Game.level.endPoint = level.endPoint;
	Game.level.pipeBank.inventory = JSON.parse( JSON.stringify( level.inventory ) );
	Game.placedPipe = [
		[2, -1]
	];
};

function gifGo() {
	setTimeout( function() {
		$( '#intro' ).hide();
		$( '#main-canvas' ).show();
		Game.state = 'start';
	}, 7350 );
};

function assignImages() {
	// Grab images
	Game.images.test = $( '#test-img' )[0];
	Game.images.rock = $( '#rock-img' )[0];
	Game.images.background = $( '#background-img' )[0];
	Game.images.compGrid = $( '#comp-grid' )[0];
	Game.images.compTop = $( '#comp-top' )[0];
	Game.images.pipeBottomLeft = $( "#pipe-bl" )[0];
	Game.images.pipeBottomRight = $( "#pipe-br" )[0];
	Game.images.pipeHorizontal = $( "#pipe-h" )[0];
	Game.images.pipeVertical = $( "#pipe-v" )[0];
	Game.images.pipeTopLeft = $( "#pipe-tl" )[0];
	Game.images.pipeTopRight = $( "#pipe-tr" )[0];
	Game.images.pipeSideBottomRight = $( "#pipe-side-br" )[0];
	Game.images.pipeSideVertical = $( "#pipe-side-v" )[0];
	Game.images.compTutorial = $( "#comp-tutorial" )[0];
	Game.images.compWin = $( "#comp-win" )[0];
	Game.images.compLose = $( "#comp-lose" )[0];
};

function setGameMouse( event ) {
	if( event.originalEvent.touches && event.originalEvent.touches[0] ) {
		Game.mouseX = event.originalEvent.touches[0].pageX;
		Game.mouseY = event.originalEvent.touches[0].pageY;
	} else {
		Game.mouseX = event.pageX;
		Game.mouseY = event.pageY;
	}
};

function controls() {
	$( document ).on( 'touchstart mousedown tap taphold', function( event ) {
		switch( Game.state ) {
			case 'logo':
				break;
			case 'start':
				Game.state = 'ingame';
				// Start everything
				startGame( Level1 );
				break;
			case 'losescreen':
				Game.state = 'start';
				break;
			case 'winscreen':
				// load next level
				startGame( Level2 );
				break;
			case 'ingame':
				setGameMouse( event );
				if( Game.cursorState === null ) {
					// if click on lastplaced pipe, refund it
					var screenCol = Math.floor( Game.mouseX / Game.tileWidth );
					var screenRow = Math.floor( Game.mouseY / Game.tileWidth );
					var onBoard = true;
					if( screenRow < 2 || screenCol == 0 || screenCol == 11 ) { onBoard = false;}
					if( onBoard &&
						Game.placedPipe[Game.placedPipe.length - 1][0] == screenRow &&
						Game.placedPipe[Game.placedPipe.length - 1][1] == screenCol - 1 ) {
						Game.placedPipe.pop();
						var refund = Game.level.layout[screenRow][screenCol - 1];
						Game.level.layout[screenRow][screenCol - 1] = null;
						Game.level.pipeBank.inventory[refund] = Game.level.pipeBank.inventory[refund] + 1;
						Game.playSound( 'pickup' );
					}
				}
				// If not on a bank piece, return;
				var bankChoice = Game.level.pipeBank.intersects( Game.mouseX, Game.mouseY );
				if( bankChoice != null ) {
					if( Game.cursorState != null ) {
						Game.level.pipeBank.inventory[Game.cursorState] = Game.level.pipeBank.inventory[Game.cursorState] + 1;
						Game.cursorState = null;
					}
					console.log( "Picked up piece: " + bankChoice );
					if( Game.level.pipeBank.inventory[bankChoice] > 0 ) {
						Game.cursorState = bankChoice;
						Game.level.pipeBank.inventory[bankChoice] = Game.level.pipeBank.inventory[bankChoice] - 1;
					}
				}
				break;
		}

	} );

	$( document ).on( 'touchmove mousemove', function( event ) {
		setGameMouse( event );
	} );

	$( document ).on( 'touchend mouseup', function( event ) {
		switch( Game.state ) {
			case 'start':
				break;
			case 'losescreen':
				break;
			case 'winscreen':
				// load next level
				// goto ingame
				break;
			case 'ingame':
				if( Game.cursorState === null ) { return; }
				setGameMouse( event );

				if( Game.cursorState !== null ) {
					// try to place pipe
					var screenCol = Math.floor( Game.mouseX / Game.tileWidth );
					var screenRow = Math.floor( Game.mouseY / Game.tileWidth );
					if( screenRow < 2 || screenCol == 0 || screenCol == 11 ) { return; }
					var isPlaced = placeTile( screenRow, screenCol - 1 );
				}
				break;
		}

	} );
};

function dropCursorItem() {
	// Drop item back to inventory
	Game.level.pipeBank.inventory[Game.cursorState] = Game.level.pipeBank.inventory[Game.cursorState] + 1;
	Game.cursorState = null;
}
function placeTile( row, col ) {
	if( Game.cursorState == null ) { return false; }
	if( Game.level.layout[row][col] === 'rock' ) { return false; }
	var lastPlaced = Game.placedPipe[Game.placedPipe.length - 1];
	var rowDiff = Math.abs( row - lastPlaced[0] );
	var colDiff = Math.abs( col - lastPlaced[1] );
	if( rowDiff + colDiff !== 1 ) {
		dropCursorItem();
		return false;
	}
	// Special check for starting pipe
	var endPipe;
	if( lastPlaced[0] === 2 && lastPlaced[1] == -1 ) {
		endPipe = 'pipeHorizontal';
	} else {
		endPipe = Game.level.layout[lastPlaced[0]][lastPlaced[1]];
	}
	// Check legal pipery
	var isLegal = willItPipe( endPipe, Game.cursorState, row - lastPlaced[0], col - lastPlaced[1] );
	if( !isLegal ) {
		dropCursorItem();
		Game.playSound( 'wrong' );
		return false;
	}
	Game.level.layout[row][col] = Game.cursorState;
	Game.placedPipe.push( [row, col] );
	Game.cursorState = null;
	Game.playSound( 'build' );
	checkWin();
	return true;
};

function willItPipe( pipe1, pipe2, pipe2Vertical, pipe2Horizontal ) {
	var pipe1Direction;
	var pipe2Direction;
	pipe2Direction = pipe2Vertical == 1 ? 'bottom' :
		( pipe2Vertical == -1 ? 'top' :
			( pipe2Horizontal == 1 ? 'right' : 'left' ) );
	pipe1Direction = pipe2Direction == 'left' ? 'right' :
		( pipe2Direction == 'right' ? 'left' :
			( pipe2Direction == 'top' ? 'bottom' : 'top'));
	if( pipeRelations[pipe1].indexOf( pipe2Direction ) != -1 &&
		pipeRelations[pipe2].indexOf( pipe1Direction ) != -1 ) { return true;}
	return false

};

function attachResize() {
	$( window ).resize( _.debounce( function() {
		fitCanvasToScreen();
	}, 500 ) );
};

function checkWin() {
	var lastPlaced = Game.placedPipe[Game.placedPipe.length - 1];
	// Endpoint number is offset by tileheight of top baner (2) and 9th col is last col in gameboard
	if( lastPlaced[0] == Game.level.endPoint + 2 && lastPlaced[1] == 9 ) {
		// Check if they legally join
		if( ['pipeHorizontal', 'pipeTopLeft', 'pipeBottomLeft'].indexOf( Game.level.layout[lastPlaced[0]][lastPlaced[1]] ) != -1 ) {
			Timer.pause();
			Game.stopSound( 'bgmusic' );
			Game.playSound( 'victory' );
			Game.state = 'winscreen';
			$( '#top-gif-img' ).toggleClass( 'active inactive' );
			$( '#comp-win-img' ).show();
			clearInterval( Timer.updateId );
		}
	}
};

function fitCanvasToScreen() {
	// Get most constraining dimension
	var heightConstraintCheck = $( 'body' ).height() * 1.5;
	var widthConstraintCheck = $( 'body' ).width();
	// Max size canvas given out constraining dimension
	var height = $( 'body' ).height() % 2 == 0 ? $( 'body' ).height() : $( 'body' ).height() - 1;
	var width = $( 'body' ).width() % 2 == 0 ? $( 'body' ).width() : $( 'body' ).width() - 1;
	if( heightConstraintCheck < widthConstraintCheck ) {
		$( '#main-canvas' ).attr( 'height', height + 'px' );
		Game.canvasHeight = height;
		Game.tileWidth = height / Game.unitHeight;
		$( '#main-canvas' ).attr( 'width', (height * 1.5) + 'px' );
		Game.canvasWidth = (height * 1.5);
	} else {
		$( '#main-canvas' ).attr( 'width', width + 'px' );
		Game.canvasWidth = width;
		$( '#main-canvas' ).attr( 'height', (width / 1.5) + 'px' );
		Game.canvasHeight = (width / 1.5);
		Game.tileWidth = width / Game.unitWidth;

	}

	$( '#intro' ).width( Game.canvasWidth + 'px' );
	$( '#intro' ).height( Game.canvasHeight + 'px' );

	$( '#top-gif-img' ).width( Game.canvasWidth + 'px' );
	$( '#top-gif-img' ).height( (Game.canvasHeight * Game.topGifRatio) + 'px' );
	$( '#comp-win-img' ).width( (Game.canvasWidth * 0.75) + 'px' );
	$( '#comp-win-img' ).height( (Game.canvasWidth * 0.75) * (405 / 768) + 'px' );
	$( '#comp-lose-img' ).width( (Game.canvasWidth * 0.75) + 'px' );
	$( '#comp-lose-img' ).height( (Game.canvasWidth * 0.75) * (405 / 768) + 'px' );
	fitBankDimensions();
	Timer.resizeMe( Game.canvasWidth, Game.canvasHeight );
};

function fitBankDimensions() {
	var pipeBank = Game.level.pipeBank;
	var bankHeight = Game.canvasHeight * pipeBank.bankHeight;
	var bankIconSize = (bankHeight - (7 * pipeBank.bankIconGap * Game.canvasHeight)) / 6;

	var bankX = pipeBank.bankLeftOffset * Game.canvasWidth;
	var bankY = pipeBank.bankTopOffset * Game.canvasHeight;
	var bankWidth = pipeBank.bankWidth * Game.canvasWidth;

	Game.level.pipeBank.dimensions.x = bankX;
	Game.level.pipeBank.dimensions.y = bankY;
	Game.level.pipeBank.dimensions.height = bankHeight;
	Game.level.pipeBank.dimensions.width = bankWidth;
	Game.level.pipeBank.dimensions.iconSize = bankIconSize;
	Game.level.pipeBank.dimensions.bottom = bankY + bankHeight;
	Game.level.pipeBank.dimensions.right = bankX + bankWidth;
};